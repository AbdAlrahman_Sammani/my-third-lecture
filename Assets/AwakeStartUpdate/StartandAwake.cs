﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartandAwake : MonoBehaviour {
    public void Awake()
    {
        Debug.Log("Awake");
    }

    void OnEnable()
    {
        Debug.Log("OnEnabel");
    }
    void OnDisable()
    {
        Debug.Log("OnDisabled");
    }
	void Start () {
        Debug.Log("Start");
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Update=" + Time.deltaTime);
	}
    public void FixedUpdate()
    {
        Debug.Log("FixedUpdate=" + Time.deltaTime);
    }
    void LateUpdate()
    {
        Debug.Log("LateUpdate=" + Time.deltaTime);
    }
}
