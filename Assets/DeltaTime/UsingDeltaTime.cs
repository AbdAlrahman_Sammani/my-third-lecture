﻿using UnityEngine;
using System.Collections;

public class UsingDeltaTime : MonoBehaviour
{
    public float speed = 8f;
    public float countdown = 3.0f;
    public Light dlight;

    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0.0f)
            dlight.enabled = true;

        if (Input.GetKey(KeyCode.RightArrow))
            transform.position += new Vector3(speed * Time.deltaTime, 0.0f, 0.0f);
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(new Vector3(-speed*Time.deltaTime, 0, 0));
        }
        /// try it without time.deltaTime
    }
}