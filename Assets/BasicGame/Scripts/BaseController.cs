﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour {

    public float baseSpeed;
    public bool enableAi;
    public GameObject balGameObject;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Move();
        if (enableAi) BaseAI();
	}
    //(1)
    public void Move()
    {
        ///(1)
     /*   if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.Translate(new Vector3(-baseSpeed, 0, 0));

        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.Translate(new Vector3(baseSpeed, 0, 0));
        }*/
        //(2)
         if (Input.GetKey(KeyCode.LeftArrow))
          {
              transform.Translate(new Vector3(-baseSpeed*Time.deltaTime, 0, 0));

          }
          if (Input.GetKey(KeyCode.RightArrow))
          {
              transform.Translate(new Vector3(baseSpeed*Time.deltaTime, 0, 0));
          }
        transform.position =new Vector3( Mathf.Clamp(transform.position.x, -4.7f, 4.7f),transform.position.y,transform.position.z);
    }
    public void BaseAI()
    {
        this.transform.position = new Vector3(balGameObject.transform.position.x, transform.position.y, transform.position.z);
    }
}
