﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallContoller : MonoBehaviour {
    private Rigidbody rigidB;
    public bool ballInPlay;
    public float ballInitialVelocity = 600f;
    Vector3 ballInitPosition;
	// Use this for initialization
	void Start () {
        rigidB = GetComponent<Rigidbody>();
        ballInitPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        LunchTheBall();
        FixBallStuck();
	}
    public void LunchTheBall()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (!ballInPlay)
            {
               
                ballInPlay = true;
                rigidB.isKinematic = false;
                rigidB.AddForce(new Vector3(ballInitialVelocity, ballInitialVelocity, 0));
            }
        }
    }
    public void FixBallStuck()
    {
        Vector3 tweak = new Vector3(Random.Range(.1f, .2f), Random.Range(.1f, .2f), transform.position.z);
        rigidB.velocity += tweak;
    }
    public void ResetBall()
    {
        ballInPlay = false;
        rigidB.velocity = new Vector3(0, 0, 0);
        rigidB.isKinematic = true;
        transform.position = ballInitPosition;
    }
}
