﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickManager : MonoBehaviour {
    public GameObject particleGameObject;


     public void OnCollisionEnter(Collision other)
    {
        Instantiate(particleGameObject, transform.position, Quaternion.identity);
        GameManager.instance.DestroyBrick();
        GameManager.instance.ScoreHandler();
        Destroy(this.gameObject);
    }
}
