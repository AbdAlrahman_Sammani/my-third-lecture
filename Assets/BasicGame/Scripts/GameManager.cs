﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour {
    public int livesCount = 5;
    public int bricksCount = 20;
    public float resetDelay = 1f;
    public int score = 0;
    public Text livesText;
    public Text scoreText;
    public GameObject baseprefab;
    public GameObject cloneBase;
    public GameObject gameOver;
    public GameObject brickPrefab;
    public GameObject deathParticle;
    public GameObject restartButton;
    public GameObject ballInstance;
    public static GameManager instance = null;


    // Use this for initialization
    void Start() {
        ///Singltone
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update() {

    }
   
    public void CheckGameOver()
    {
        if(bricksCount<1)
        {
            gameOver.SetActive(true);
            gameOver.GetComponentInChildren <Text>().text = "You Won";
            Time.timeScale=.25f;
            restartButton.SetActive(true);
        }
        if (livesCount < 1)
        {
            gameOver.SetActive(true);
            gameOver.GetComponentInChildren<Text>().text = "You Lose";
            Time.timeScale = .25f;
            restartButton.SetActive(true);
        }
    }
    public void ResetGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);        
    }
    public void LoseLife()
    {
        livesCount--;
        livesText.text = "Lives=" + livesCount;
        Instantiate(deathParticle, cloneBase.transform.position, Quaternion.identity);
    }
   
    public void DestroyBrick()
    {
        bricksCount--;
        CheckGameOver();
    }
    public void ScoreHandler()
    {
        score++;
        scoreText.text = "Score" + score;
    }
    public void DieHandler()
    {
        LoseLife();
        ballInstance.GetComponent<BallContoller>().ResetBall();
        baseprefab.transform.position = new Vector3(0, -7.786f, 0);
        CheckGameOver();
    }
    
}
