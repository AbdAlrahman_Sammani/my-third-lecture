﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lerp : MonoBehaviour {
    public Light light;
    float val;
    public GameObject gobject;
    public Vector3 startPos;
    public Vector3 desiredPosition;
	// Use this for initialization
	void Start () {
        startPos = gobject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (val > 1) val = 0;
       val += Mathf.Lerp(0, 1, Time.deltaTime);
        light.intensity = val;

        ///
        gobject.transform.position = Vector3.Lerp(gobject.transform.position, desiredPosition, Time.deltaTime);
        gobject.transform.position = Vector3.Slerp(gobject.transform.position, desiredPosition, Time.deltaTime);

    }
}
